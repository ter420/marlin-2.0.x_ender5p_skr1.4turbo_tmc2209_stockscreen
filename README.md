## Ender 5 Plus + Marlin 2.0 DW6 + SKR1.4Turbo + TMC2209 + Stock Screen

## Update V1.0 Beta

- fix z not comming home, now just smooth

## Primary notes for DW6

- Power Loss Recovery is functional again
- Added screen for manual PID entry and custom tuning
- Added screen for Step/mm and probe offset editing
- Base bumped to Marlin 2.0.4.4
- Tweaks to limit RAM consumption where possible

## Here is how to..

1. Print your mounting bracket [https://www.thingiverse.com/thing:4251738](https://www.thingiverse.com/thing:4251738)

2) Pins and wiring for SKR 1.4 and 1.4Turbo in this video [www.youtube.com/watch?v=-Y18Rz8LlSY&t=2864s](https://www.youtube.com/watch?v=-Y18Rz8LlSY&t=2864s)

![](images/Wiring.jpg)
recheck the Cable color for BLtouch
![](images/BLtouch.png)

3. Screen Wiring (if not working try to swap TX/RX) [https://www.deviousweb.com/2020/05/08/ender-5-plus-skr1-3-and-factory-screen/?fbclid=IwAR2T2X9YodPXYFJPVses-2eudmR7yOTyLxouJP0E0bV7vetxh1kzRB1QPnIs](https://www.deviousweb.com/2020/05/08/ender-5-plus-skr1-3-and-factory-screen/?fbclid=IwAR2T2X9YodPXYFJPVses-2eudmR7yOTyLxouJP0E0bV7vetxh1kzRB1QPnIs)

![](images/screen1.jpg)
![](images/screen2.jpg)

4. Fix TMC2209 Bug by Cut pin and Cap for UARP mode
   ![](images/Capandpincut.jpg)

5. Flash Stock Screen with "StockScreen_CR-XABL V2.zip"

- Prep the SD card
  Your printer came with an 8GB microSD card, and you need to repurpose and prep it to flash the screen:
  1a) backup the contents of the SD card
  1b) format it to FAT32 with 4KB (4096B) clusters. !!!DON'T SKIP THIS!!! If you don't do this, it won't work. 8K clusters wont work. 16K clusters wont work. 32K clusters won't work. Format it to 4kb clusters. That's why you use the 8GB creality card; it's small enough to do this with.
- Extrac and put file in "StockScreen_CR-XABL V2.zip" to SD card
- with the machine off, insert the SD card into the SD card slot on the back of the screen
- Turn the machine on and watch the screen. it will be blue with writing on it and some counters populating down the right column. After that, many images will flash. Finally, the top line of the screen will end in "...END!" (2-3 min)
- turn the machine off
- remove the SD card from the back of the screen
- reassemble the control box

6. Build or Upload Firmware with "LPC1769" on PlatformIO in VSCode (make sure you compile with "LPC1769")

- [https://www.deviousweb.com/2020/05/08/ender-5-plus-skr1-3-and-factory-screen/?fbclid=IwAR2T2X9YodPXYFJPVses-2eudmR7yOTyLxouJP0E0bV7vetxh1kzRB1QPnI ](https://www.deviousweb.com/2020/05/08/ender-5-plus-skr1-3-and-factory-screen/?fbclid=IwAR2T2X9YodPXYFJPVses-2eudmR7yOTyLxouJP0E0bV7vetxh1kzRB1QPnI)
- [www.youtube.com/watch?v=-Y18Rz8LlSY&t=2864s](https://www.youtube.com/watch?v=-Y18Rz8LlSY&t=2864s)

![](images/LPC1679.jpg)

## Note :

in Configuration.h
Edit comment in //#define MelziHostOnly
if you want to print from your SD Card

![](images/S__5595138.jpg)

---

Thanks to

- [InsanityAutomation for working marlin with stock screen](https://github.com/InsanityAutomation/Marlin)
- [Jimmy White for porting to SKR1.3](https://www.deviousweb.com/2020/05/08/ender-5-plus-skr1-3-and-factory-screen/?fbclid=IwAR2T2X9YodPXYFJPVses-2eudmR7yOTyLxouJP0E0bV7vetxh1kzRB1QPnI)
- [S13Tyler for fix BLT working](https://github.com/bigtreetech/BIGTREETECH-SKR-V1.3/issues/207?fbclid=IwAR3qElSQzMajyqveZ26Wnx-Y7785tNw6uAxyFhWWsip2PXcg4jFJEV63_kc)
- [How to Flash Screen](https://www.facebook.com/groups/1223729471113647/permalink/1492293670923891/)
